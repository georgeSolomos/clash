package Clash;

public class OneHandedWeapon extends Card{
	private int damage;
	private int agility = 0;
	private int durability;
	
	public OneHandedWeapon(String name, int damage, int agility){
		super(name);
		this.damage = damage;
		this.agility = agility;
		durability = 100;
	}
	
	public OneHandedWeapon(String name, int damage){
		super(name);
		this.damage = damage;
		durability = 100;
	}
	
	public @Override String toString(){
		if(agility == 0) return "Damage: " + damage + ", Durability: " + durability;
		else return "Damage: " + damage + ", Agility: " + agility + ", Durability: " + durability;
	}
	
	public int getDamage(){
		return damage;
	}
	
	public int getAgility(){
		return agility;
	}

	public int getDurability(){
		return durability;
	}
	
	public void offsetDurability(int durability_offset){
		durability += durability_offset;
	}
}