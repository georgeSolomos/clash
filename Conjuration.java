package Clash;

import java.util.Random;

public class Conjuration extends Card{
	private int type;
	private int damage;
	private int mana_cost;
	private String effect = null;
	/* types are:
	1 - fire tornado
	2 - icicle burst
	3 - big bang
	4 - sludge bomb
	5 - summon zombie horde
	6 - stampede */
	public Conjuration(String name, int type, int damage, int mana_cost, String effect){
		super(name);
		this.type = type;
		this.damage = damage;
		this.mana_cost = mana_cost;
		this.effect = effect;
	}
	
	public Conjuration(String name, int type, int damage, int mana_cost){
		super(name);
		this.type = type;
		this.damage = damage;
		this.mana_cost = mana_cost;
	}
	
	public @Override String toString(){
		if(effect != null) return "Damage: " + damage + ", Mana cost: " + mana_cost + " (" + effect + ")";
		else return "Damage: " + damage + ", Mana cost: " + mana_cost;
	}
	
	public int getDamage(){
		return damage;
	}
	
	public int getManaCost(){
		return mana_cost;
	}
	
	public int getType(){
		return type;
	}
}