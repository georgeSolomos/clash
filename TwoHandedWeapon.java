package Clash;

public class TwoHandedWeapon extends Card{
	private int damage;
	private int durability;
	
	public TwoHandedWeapon(String name, int damage){
		super(name);
		this.damage = damage;
		durability = 150;
	}
	
	public @Override String toString(){
		return "Damage: " + damage + ", Durability: " + durability;
	}
		
	public int getDamage(){
		return damage;
	}
	
	public int getDurability(){
		return durability;
	}
	
	public void offsetDurability(int durability_offset){
		durability += durability_offset;
	}
}