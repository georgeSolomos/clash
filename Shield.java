package Clash;

public class Shield extends Card{
	private int resistance;
	private int durability;
	
	public Shield(String name, int resistance){
		super(name);
		this.resistance = resistance;
		durability = 200;
	}
	
	public @Override String toString(){
		return "Resistance: " + resistance + ", Durability: " + durability;
	}
	
	public int getresistance(){
		return resistance;
	}
	
	public int getDurability(){
		return durability;
	}
	
	public void offsetDurability(int durability_offset){
		durability += durability_offset;
	}
}