Welcome to Clash
-----------------------------
This repo holds the source files for the console-only version of the medieval fantasy card game Clash.
Compile with 'javac *.java' and run with 'java Game'.