package Clash;

public class Armour extends Card{
	private int resistance;
	private int agility;
	private int type;
	private int durability;
	/* types are:
	1 - cuirass
	2 - helm
	3 - gauntlets
	4 - greaves */
	public Armour(String name, int resistance, int agility, int type){
		super(name);
		this.resistance = resistance;
		this.agility = agility;
		this.type = type;
		durability = 250;
	}
	
	public @Override String toString(){
		return "Resistance: " + resistance + ", Agility: " + agility + ", Durability: " + durability;
	}
	
	public int getResistance(){
		return resistance;
	}
	
	public int getAgility(){
		return agility;
	}
	
	public int getType(){
		return type;
	}
	
	public int getDurability(){
		return durability;
	}
	
	public void offsetDurability(int durability_offset){
		durability += durability_offset;
	}
}