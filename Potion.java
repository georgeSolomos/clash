package Clash;

public class Potion extends Card{
	private int type;
	private String description;
	/* types are:
	1 - restore health I
	2 - restore health II
	3 - restore health III
	4 - restore mana I
	5 - restore mana II
	6 - restore mana III
	7 - enhance agility
	8 - inflict poison
	9 - inflict paralysis
	10 - inflict osteoporosis */
	public Potion(String name, int type, String description){
		super(name);
		this.type = type;
		this.description = description;
	}
	
	public @Override String toString(){
		return description;
	}
	
	public int getType(){
		return type;
	}
}