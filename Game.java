package Clash;
import java.util.Scanner;
import java.io.Console;

public class Game{
	private static int turn_count = 0;
	private static boolean attacked = false;
	
	public static void action(Player player, Player opponent, boolean p1){
		player.isPoisoned();
		player.isParalysed();
		player.hasBrokenBone();
		player.hasEnhancedAgility();
		if(turnNo() != 0){
			player.offsetMana(50);
		}
		// show health, mana and ailments
		player.printStatus();
		// show all slots
		player.printEquipList();
		System.out.println();
		Scanner in = new Scanner(System.in);
		boolean hand = true, end = false;
		attacked = false;
		while(end == false){
			if(hand == true){
				player.showHand();
			}
			System.out.println();
			System.out.print("(h for help) > ");
			String input = in.next();
			/*
			 * END TURN
			 */
			if(input.equals("end")){
				end = true;
			}
			/*
			 * EQUIP
			 */
			else if(input.equals("e")){
				if(in.hasNextInt()){
					int index = in.nextInt();
					if (index < 1 || index > player.cardsInHand()){
						System.out.println("Invalid card number");
						in.nextLine();
						hand = false;
					}
					else{
						Card target = player.searchFor(index);
						
						if(in.hasNextInt()){
							int slot = in.nextInt();
							
							if (slot < 1 || slot > 9){
								System.out.println("Invalid slot number");
								hand = false;
							}
							else{
								hand = true;
								switch(slot){
								case 1:
									if(!player.equipHead(target) && player.getChoice() == false){
										System.out.println("Can't equip that there");
										hand = false;
									}
									break;
								case 2:
									if(!player.equipTorso(target) && player.getChoice() == false){
										System.out.println("Can't equip that there");
										hand = false;
									}
									break;
								case 3:
									if(!player.equipLeftArm(target) && player.getChoice() == false){
										System.out.println("Can't equip that there");
										hand = false;
									}
									break;
								case 4:
									if(!player.equipRightArm(target) && player.getChoice() == false){
										System.out.println("Can't equip that there");
										hand = false;
									}
									break;
								case 5:
									if(!player.equipLeftWeapon(target) && player.getChoice() == false){
										System.out.println("Can't equip that there");
										hand = false;
									}
									break;
								case 6:
									if(!player.equipRightWeapon(target) && player.getChoice() == false){
										System.out.println("Can't equip that there");
										hand = false;
									}
									break;
								case 7:
									if(!player.equipLeftLeg(target) && player.getChoice() == false){
										System.out.println("Can't equip that there");
										hand = false;
									}
									break;
								case 8:
									if(!player.equipRightLeg(target) && player.getChoice() == false){
										System.out.println("Can't equip that there");
										hand = false;
									}
									break;
								
								case 9:
									if(!player.equipConjuration(target) && player.getChoice() == false){
										System.out.println("Can't equip that there");
										hand = false;
									}
									break;
								}
							}
							in.nextLine();
						}
						else{
							System.out.println("Invalid slot number");
							in.nextLine();
							hand = false;
						}
					}
				}
				else{
					System.out.println("Invalid card number");
					in.nextLine();
					hand = false;
				}
			}
			/*
			 * ATTACK
			 */
			else if(input.equals("a")){
				if(!attacked){
					// not paralysed
					if(!player.isParalysed()){
						// put first number
						if(in.hasNextInt()){
							int index = in.nextInt();
							if (index != 5 && index != 6 && index != 9){
								System.out.println("Invalid slot number (must attack with left weapon, right weapon or conjuration)");
								//in.nextLine();
								hand = false;
							}
							else{
								// put second number
								if(in.hasNextInt()){
									if(turnNo() == 0){
										System.out.println("Can't attack on the first turn");
										in.nextLine();
									}
									else{
										int slot = in.nextInt();
										if (slot < 1 || slot > 8 || slot == 5 || slot == 6){
											System.out.println("Invalid target slot number");
											hand = false;
										}
										else{
											Card attacker = null;
											if(index == 5){
												attacker = player.getLeftWeapon();
											}
											else if(index == 6){
												attacker = player.getRightWeapon();
											}
											else if(index == 9){
												attacker = player.getConjuration();
											}
											switch(slot){
											case 1:
												Card head = opponent.getHead();
												attack(head, attacker, player, opponent, p1);
												break;
											case 2:
												Card torso = opponent.getTorso();
												attack(torso, attacker, player, opponent, p1);
												break;
											case 3:
												Card left_arm = opponent.getLeftArm();
												attack(left_arm, attacker, player, opponent, p1);
												break;
											case 4:
												Card right_arm = opponent.getRightArm();
												attack(right_arm, attacker, player, opponent, p1);
												break;
											case 7:
												Card left_leg = opponent.getLeftLeg();
												attack(left_leg, attacker, player, opponent, p1);
												break;
											case 8:
												Card right_leg = opponent.getRightLeg();
												attack(right_leg, attacker, player, opponent, p1);
												break;
											}
										}
										//in.nextLine();
										if(opponent.getHealth() == 0){
											System.out.println();
											System.out.println("Congratulations, you win!");
											System.exit(0);
										}
										hand = true;
									}
								}
								else{
									System.out.println("Invalid target slot number");
									//in.nextLine();
									hand = false;
								}
							}
						}
						else{
							System.out.println("Invalid card number");
							in.nextLine();
							hand = false;
						}
					}
					else{
						System.out.println("Paralysed, can't attack");
					}
					in.nextLine();
					hand = false;
				}
				else{
					System.out.println("Already attacked this turn");
					in.nextLine();
					hand = false;
				}
			}
			/*
			 * USE POTION
			 */
			else if(input.equals("u")){
				if(in.hasNextInt()){
					int index = in.nextInt();
					if (index < 1 || index > player.cardsInHand()){
						System.out.println("Invalid card number");
						in.nextLine();
						hand = false;
					}
					else{
						Card target = player.searchFor(index);
						if(!player.usePotion(target, player, opponent, p1)){
							System.out.println("Can't use that as a potion");
							in.nextLine();
							hand = false;
						}
						else{
							in.nextLine();
							hand = true;
						}
					}
				}
				else{
					System.out.println("Invalid card number");
					in.nextLine();
					hand = false;
				}
			}
			/*
			 * HELP
			 */
			else if(input.equals("h")){
				System.out.println("Equipping:");
				System.out.println("Type 'e' followed by the item number in your hand, then the slot number");
				System.out.println("e.g. e 1 1");
				System.out.println("will equip the first item in your hand ('" + player.getCardAtIndex(0).getName() + "' in this case) to the head slot.");
				System.out.println("Note: Helm - head, Cuirass - torso, Greaves - legs, Gauntlets - arms.");
				System.out.println();
				System.out.println("Viewing item slots:");
				System.out.println("To view all item slots, type 's'.");
				System.out.println();
				System.out.println("Attacking:");
				System.out.println("TBA");
				System.out.println();
				System.out.println("Using a potion:");
				System.out.println("To use a potion, type 'u' followed by the item number in your hand.");
				System.out.println();
				System.out.println("Status effects:");
				System.out.println("Poison: Takes 5 health off each turn and lasts for 5 turns.");
				System.out.println("Paralysis: Stops you from attacking for 1 turn.");
				System.out.println("Broken bone: Reduces agility to 0 for 2 turns.");
				System.out.println();
				System.out.println("Ending your turn:");
				System.out.println("To end your turn, type 'end'.");
				System.out.println();
				System.out.println("Ending the game:");
				System.out.println("To end the game, type 'exit' and confirm with 'y'.");
				in.nextLine();
				hand = false;
			}
			/*
			 * SHOW EQUIP LIST
			 */
			else if(input.equals("s")){
				player.printEquipList();
				in.nextLine();
				hand = false;
			}
			/*
			 * EXIT
			 */
			else if(input.equals("exit")){
				System.out.print("Are you sure? (y/n) ");
				String confirm = in.next();
				if(confirm.equals("y")){
					System.exit(0);
				}
			}
			else{
				System.out.println("Invalid command");
				in.nextLine();
				hand = false;
			}
		}
	}
	
	public static void attack(Card body_part, Card attacker, Player player, Player opponent, boolean p1){
		double resistance = 20, attack = 0;
		int damage = 0;
		double AonR = 0;
		if(body_part != null){
			resistance = ((Armour)body_part).getResistance();
		}
		if(attacker != null){
			if(attacker.getClass().getName().equals("Clash.OneHandedWeapon")){
				attack = ((OneHandedWeapon)attacker).getDamage();
			}
			if(attacker.getClass().getName().equals("Clash.TwoHandedWeapon")){
				attack = ((TwoHandedWeapon)attacker).getDamage();
			}
			if(attacker.getClass().getName().equals("Clash.Conjuration")){
				attack = ((Conjuration)attacker).getDamage();
				player.useConjuration(attacker, player, opponent, p1);
			}
			AonR = attack / resistance;
			damage = (int)(Math.round(AonR * 30));
			opponent.offsetHealth(-damage);
			System.out.print("Hit! Player 2's health is now " + opponent.getHealth());
			System.out.println();
			attacked = true;
		}
		else{
			System.out.println("You have nothing equipped there");
			System.out.println();
			attacked = false;
		}
	}
	
	public static void initialDraw(Player player){
		for (int i = 0; i < 5; i++){
			player.draw(player);
		}
	}
	
	public static int turnNo(){
		return turn_count;
	}
	
	/*public void clearConsole(){
		Console console = System.console();        
		if(console == null)
			System.out.println("Couldn't get Console object");
		console.flush();
	}*/
	
	public void clearConsole(){
		for(int i = 0; i < 50; i++){
			System.out.println();
		}
		/*try{
			Thread.sleep(2000);
		}
		catch(InterruptedException ie){
			System.out.println("Interrupted exception when trying to clear console");
		}*/
	}
	
	public static void main(String[] args){
		Player p1 = new Player();
		Player p2 = new Player();
		System.out.println("********** Welcome to Clash! **********");
		
		while(true){
			System.out.println("----------     Player 1:     ----------");
			// initial draw for player 1
			if(turn_count == 0){
				initialDraw(p1);
			}
			else{
				if(!p1.draw(p1)){
					System.out.println("Out of cards, game finished");
					// Print end of game stats and who won
					System.exit(0);
				}
			}
			// action
			action(p1, p2, true);
			new Game().clearConsole();
			System.out.println("Player 1's status:");
			p1.printStatus();
			p1.printEquipList();
			System.out.println();
			System.out.println("----------     Player 2:     ----------");
			// initial draw for player 2
			if(turn_count == 0){
				initialDraw(p2);
			}
			else{
				if(!p2.draw(p2)){
					System.out.println("Out of cards, game finished");
					// Print end of game stats and who won
					System.exit(0);
				}
			}
			// action
			action(p2, p1, false);
			turn_count++;
			new Game().clearConsole();
			System.out.println("Player 2's status:");
			p2.printStatus();
			p2.printEquipList();
			System.out.println();
		}
	}
}