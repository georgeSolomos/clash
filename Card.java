package Clash;

public class Card{
	private String name;
	
	public Card(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public @Override String toString(){
          return name;
    }
}