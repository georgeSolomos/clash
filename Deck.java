package Clash;

import java.util.ArrayList;
import java.util.Random;

public class Deck{
	private Card scimitar = new OneHandedWeapon("Scimitar", 55, 0);
	private Card estoc = new OneHandedWeapon("Estoc", 60, 0);
	private Card kusanagi = new OneHandedWeapon("Kusanagi", 80, 0);
	private Card tomahawk = new OneHandedWeapon("Tomahawk", 65, 0);
	private Card misericorde = new OneHandedWeapon("Misericorde", 40, 6);
	private Card dagger = new OneHandedWeapon("Rondel dagger", 30, 10);
	
	private Card longsword = new TwoHandedWeapon("Longsword", 110);
	private Card katana = new TwoHandedWeapon("Katana", 120);
	private Card claymore = new TwoHandedWeapon("Claymore", 140);
	private Card battleaxe = new TwoHandedWeapon("Battleaxe", 140);
	private Card qiang = new TwoHandedWeapon("Qiang", 130);
	private Card longbow = new TwoHandedWeapon("English longbow", 100);
	
	private Card hoplon = new Shield("Hoplon", 30);
	private Card buckler = new Shield("Buckler", 40);
	private Card scutum = new Shield("Scutum", 50);
	
	private Card leather_cuirass = new Armour("Boiled leather cuirass", 50, 5, 1);
	private Card leather_helm = new Armour("Boiled leather helm", 40, 10, 2);
	private Card leather_gauntlets = new Armour("Boiled leather gauntlets", 30, 20, 3);
	private Card leather_greaves = new Armour("Boiled leather greaves", 25, 20, 4);
	private Card splint_cuirass = new Armour("Splint cuirass", 55, 2, 1);
	private Card splint_helm = new Armour("Splint helm", 45, 8, 2);
	private Card splint_gauntlets = new Armour("Splint gauntlets", 35, 18, 3);
	private Card splint_greaves = new Armour("Splint greaves", 35, 25, 4);
	private Card mail_cuirass = new Armour("Plated mail cuirass", 65, 4, 1);
	private Card mail_helm = new Armour("Plated mail helm", 50, 8, 2);
	private Card mail_gauntlets = new Armour("Plated mail gauntlets", 38, 16, 3);
	private Card mail_greaves = new Armour("Plated mail greaves", 35, 16, 4);
	private Card scale_cuirass = new Armour("Scale cuirass", 75, 3, 1);
	private Card scale_helm = new Armour("Scale helm", 60, 6, 2);
	private Card scale_gauntlets = new Armour("Scale gauntlets", 40, 12, 3);
	private Card scale_greaves = new Armour("Scale greaves", 45, 12, 4);
	private Card steel_cuirass = new Armour("Steel plate cuirass", 80, 2, 1);
	private Card steel_helm = new Armour("Steel plate helm", 70, 4, 2);
	private Card steel_gauntlets = new Armour("Steel plate gauntlets", 50, 8, 3);
	private Card steel_greaves = new Armour("Steel plate greaves", 45, 8, 4);
	
	private Card healthI = new Potion("Restore health I", 1, "Restores 50 health");
	private Card healthII = new Potion("Restore health II", 2, "Restores 80 health");
	private Card healthIII = new Potion("Restore health III", 3, "Restores 120 health");
	private Card manaI = new Potion("Restore mana I", 4, "Restores 100 mana");
	private Card manaII = new Potion("Restore mana II", 5, "Restores 180 mana");
	private Card manaIII = new Potion("Restore mana III", 6, "Restores 300 mana");
	private Card agility = new Potion("Enhance agility", 7, "Increases agility by 10 for 3 turns");
	private Card poison = new Potion("Inflict poison", 8, "Poisons the enemy");
	private Card paralysis = new Potion("Inflict paralysis", 9, "Paralyses the enemy");
	private Card osteoporosis = new Potion("Inflict osteoporosis", 10, "Breaks one of your enemy's bones");
	
	private Card fire = new Conjuration("Fire Tornado", 1, 50, 300);
	private Card ice = new Conjuration("Icicle Burst", 2, 40, 250);
	private Card bang = new Conjuration("Big Bang", 3, 100, 500);
	private Card sludge = new Conjuration("Sludge Bomb", 4, 20, 250, "50% chance of poisoning the enemy");
	private Card zombie = new Conjuration("Summon Zombie Horde", 5, 30, 250, "50% chance of paralysing the enemy");
	private Card stampede = new Conjuration("Stampede", 6, 40, 400, "50% chance of broken bone for the enemy");
	
	private ArrayList<Card> deck;

	public Deck(){
		generateDeck();
	}

	public void generateDeck(){
		deck = new ArrayList<Card>();
		int index_1, index_2;
		Random gen = new Random();
		Card temp;

		deck.add(scimitar);
		deck.add(estoc);
		deck.add(kusanagi);
		deck.add(tomahawk);
		deck.add(misericorde);
		deck.add(dagger);
		deck.add(longsword);
		deck.add(katana);
		deck.add(claymore);
		deck.add(battleaxe);
		deck.add(qiang);
		deck.add(longbow);
		deck.add(hoplon);
		deck.add(buckler);
		deck.add(scutum);
		deck.add(leather_cuirass);
		deck.add(leather_helm);
		deck.add(leather_gauntlets);
		deck.add(leather_greaves);
		deck.add(splint_cuirass);
		deck.add(splint_helm);
		deck.add(splint_gauntlets);
		deck.add(splint_greaves);
		deck.add(mail_cuirass);
		deck.add(mail_helm);
		deck.add(mail_gauntlets);
		deck.add(mail_greaves);
		deck.add(scale_cuirass);
		deck.add(scale_helm);
		deck.add(scale_gauntlets);
		deck.add(scale_greaves);
		deck.add(steel_cuirass);
		deck.add(steel_helm);
		deck.add(steel_gauntlets);
		deck.add(steel_greaves);
		deck.add(healthI);
		deck.add(healthII);
		deck.add(healthIII);
		deck.add(manaI);
		deck.add(manaII);
		deck.add(manaIII);
		deck.add(agility);
		deck.add(poison);
		deck.add(paralysis);
		deck.add(osteoporosis);
		deck.add(fire);
		deck.add(ice);
		deck.add(bang);
		deck.add(sludge);
		deck.add(zombie);
		deck.add(stampede);

		for (int i = 0; i < 500; i++)
		{
			index_1 = gen.nextInt(deck.size());
			index_2 = gen.nextInt(deck.size());

			temp = (Card)deck.get(index_2);
			deck.set(index_2, deck.get(index_1));
			deck.set(index_1, temp);
		}
	}
	
	public void generateDeck2(Player player){
		//deck = new ArrayList<Card>();
		int index_1, index_2;
		Random gen = new Random();
		Card temp;

		deck.add(scimitar);
		deck.add(estoc);
		deck.add(kusanagi);
		deck.add(tomahawk);
		deck.add(misericorde);
		deck.add(dagger);
		deck.add(longsword);
		deck.add(katana);
		deck.add(claymore);
		deck.add(battleaxe);
		deck.add(qiang);
		deck.add(longbow);
		deck.add(hoplon);
		deck.add(buckler);
		deck.add(scutum);
		deck.add(leather_cuirass);
		deck.add(leather_helm);
		deck.add(leather_gauntlets);
		deck.add(leather_greaves);
		deck.add(splint_cuirass);
		deck.add(splint_helm);
		deck.add(splint_gauntlets);
		deck.add(splint_greaves);
		deck.add(mail_cuirass);
		deck.add(mail_helm);
		deck.add(mail_gauntlets);
		deck.add(mail_greaves);
		deck.add(scale_cuirass);
		deck.add(scale_helm);
		deck.add(scale_gauntlets);
		deck.add(scale_greaves);
		deck.add(steel_cuirass);
		deck.add(steel_helm);
		deck.add(steel_gauntlets);
		deck.add(steel_greaves);
		deck.add(healthI);
		deck.add(healthII);
		deck.add(healthIII);
		deck.add(manaI);
		deck.add(manaII);
		deck.add(manaIII);
		deck.add(agility);
		deck.add(poison);
		deck.add(paralysis);
		deck.add(osteoporosis);
		deck.add(fire);
		deck.add(ice);
		deck.add(bang);
		deck.add(sludge);
		deck.add(zombie);
		deck.add(stampede);
		
		int count = 0;
		//still doesn't work properly
		for(int i = 0; i < 51; i++){
			int deck_size = deck.size();
			if(deck_size != 0){
				Card at_start = deck.get(count);
				if(player.inHand(at_start)){
					deck.remove(count);
				}
				else{
					count++;
				}
			}
		}
		
		if(deck.size() != 0){
			for (int i = 0; i < 500; i++){
				index_1 = gen.nextInt(deck.size());
				index_2 = gen.nextInt(deck.size());
	
				temp = (Card)deck.get(index_2);
				deck.set(index_2, deck.get(index_1));
				deck.set(index_1, temp);
			}
		}
	}
	
	public Card drawFromDeck(Player player){	   
		if(deck.size() == 0){
			generateDeck2(player);
			if(deck.size() == 0){
				return null;
			}
		}
		return deck.remove(0);
	}

	public int getRemainingCards(){
		return deck.size();
	}
}