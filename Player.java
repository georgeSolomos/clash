package Clash;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Player{
	private int health;
	private int mana;
	private int agility;
	private int prev_agility;
	private int added_resistance;
	private boolean poisoned = false;
	private boolean paralysed = false;
	private boolean broken_bone = false;
	private boolean enhanced_agility = false;
	private boolean choice = false;
	private int poison_end;
	private int broken_bone_end;
	private int paralysis_end;
	private int enhanced_agility_end;
	private Card head;
	private Card torso;
	private Card right_arm;
	private Card right_weapon;
	private Card left_arm;
	private Card left_weapon;
	private Card right_leg;
	private Card left_leg;
	private Card conjuration;
	private Deck deck = new Deck();
	private ArrayList<Card> hand = new ArrayList<Card>();
	
	public Player(){
		health = 1000;
		mana = 1000;
		agility = 2;
	}
	
	public int getHealth(){
		if(health > 0){
			return health;
		}
		else return 0;
	}
	
	public int getMana(){
		return mana;
	}
	
	public int getAgility(){
		return agility;
	}
	
	public int getAddedResistance(){
		return added_resistance;
	}
	
	public boolean getChoice(){
		return choice;
	}
	
	public void offsetHealth(int health_offset){
		health += health_offset;
	}
	
	public void offsetMana(int mana_offset){
		mana += mana_offset;
	}
	
	public void setAgility(int new_agility){
		if(new_agility > 100){
			agility = 100;
		}
		else if(new_agility < 0){
			agility = 0;
		}
		else{
			agility = new_agility;
		}
	}
	
	public void setPoison(boolean p1){
		if(p1){
			if (!poisoned){
				poison_end = Game.turnNo() + 4;
				poisoned = true;
			}
		}
		else{
			if (!poisoned){
				poison_end = Game.turnNo() + 5;
				poisoned = true;
			}
		}
	}
	
	public void setParalysis(boolean p1){
		if(p1){
			if (!paralysed){
				paralysis_end = Game.turnNo() + 1;
				paralysed = true;
			}
		}
		else{
			if (!paralysed){
				paralysis_end = Game.turnNo() + 2;
				paralysed = true;
			}
		}
	}
	
	public void setBrokenBone(boolean p1){
		if(p1){
			if (!broken_bone){
				prev_agility = agility;
				broken_bone_end = Game.turnNo() + 2;
				broken_bone = true;
			}
		}
		else{
			if (!broken_bone){
				prev_agility = agility;
				broken_bone_end = Game.turnNo() + 3;
				broken_bone = true;
			}
		}
	}
	
	public void isPoisoned(){
		if(poisoned){
			offsetHealth(-5);
		}
		if(Game.turnNo() == poison_end){
			poisoned = false;
		}
	}
	
	public boolean isParalysed(){
		if(Game.turnNo() == paralysis_end){
			paralysed = false;
		}
		return paralysed;
	}
	
	public void hasBrokenBone(){
		if(Game.turnNo() == broken_bone_end && Game.turnNo() != 0){
			broken_bone = false;
			setAgility(prev_agility);
		}
		if(broken_bone){
			setAgility(0);
		}
	}
	
	public void hasEnhancedAgility(){
		if(Game.turnNo() == enhanced_agility_end && Game.turnNo() != 0){
			enhanced_agility = false;
			setAgility(prev_agility);
		}
	}
	
	public boolean draw(Player player){
		Card drawn = deck.drawFromDeck(player);
		if(drawn != null){
			addToHand(drawn);
			return true;
		}
		else return false;
	}
	
	public int cardsLeft(){
		return deck.getRemainingCards();
	}
	
	public void addToHand(Card card){
		hand.add(card);
	}
	
	public int handSize(){
		return hand.size();
	}
	
	public boolean inHand(Card card){
		for(int i = 0; i < hand.size(); i++){
			String hand_card = hand.get(i).getName();
			if(hand_card.equals(card.getName())){
				return true;
			}
		}
		return false;
	}
	
	public boolean removeFromHand(Card card){
		return hand.remove(card);
	}
	
	public void showHand(){
		for(int i = 1; i <= hand.size(); i++){
			Card card = hand.get(i-1);
			String name = card.getName();
			String info = card.toString();
			String card_class = card.getClass().getName();
			if(card_class.equals("Clash.OneHandedWeapon")){
				String new_name = name.concat(" (one-handed)");
				System.out.printf("%02d. %-28s  %-28s%n",i, new_name, info);
			}
			else if(card_class.equals("Clash.TwoHandedWeapon")){
				String new_name = name.concat(" (two-handed)");
				System.out.printf("%02d. %-28s  %-28s%n",i, new_name, info);
			}
			else if(card_class.equals("Clash.Shield")){
				String new_name = name.concat(" (shield)");
				System.out.printf("%02d. %-28s  %-28s%n",i, new_name, info);
			}
			else{
				System.out.printf("%02d. %-28s  %-28s%n",i, name, info);
			}
		}
	}
	
	public int cardsInHand(){
		return hand.size();
	}
	
	public Card getHead(){
		return head;
	}
	
	public Card getTorso(){
		return torso;
	}
	
	public Card getRightArm(){
		return right_arm;
	}
	
	public Card getRightWeapon(){
		return right_weapon;
	}
	
	public Card getLeftArm(){
		return left_arm;
	}
	
	public Card getLeftWeapon(){
		return left_weapon;
	}
	
	public Card getRightLeg(){
		return right_leg;
	}
	
	public Card getLeftLeg(){
		return left_leg;
	}
	
	public Card getConjuration(){
		return conjuration;
	}
	
	public boolean equipHead(Card card){
		String card_class = card.getClass().getName();
		if(card_class.equals("Clash.Armour")){
			if(((Armour)card).getType() == 2){
				if(head != null){
					agility -= ((Armour)head).getAgility();
				}
				head = card;
				agility += ((Armour)card).getAgility();
				removeFromHand(card);
				return true;
			}
			else return false;
		}
		else return false;
	}
	
	public boolean equipTorso(Card card){
		String card_class = card.getClass().getName();
		if(card_class.equals("Clash.Armour")){
			if(((Armour)card).getType() == 1){
				if(torso != null){
					agility -= ((Armour)torso).getAgility();
				}
				torso = card;
				agility += ((Armour)card).getAgility();
				removeFromHand(card);
				return true;
			}
			else return false;
		}
		else return false;
	}
	
	public boolean equipRightArm(Card card){
		String card_class = card.getClass().getName();
		if(card_class.equals("Clash.Armour")){
			if(((Armour)card).getType() == 3){
				if(right_arm != null){
					agility -= ((Armour)right_arm).getAgility();
				}
				right_arm = card;
				agility += ((Armour)card).getAgility();
				removeFromHand(card);
				return true;
			}
			else return false;
		}
		else return false;
	}
	
	public boolean equipLeftArm(Card card){
		String card_class = card.getClass().getName();
		if(card_class.equals("Clash.Armour")){
			if(((Armour)card).getType() == 3){
				if(left_arm != null){
					agility -= ((Armour)left_arm).getAgility();
				}
				left_arm = card;
				agility += ((Armour)card).getAgility();
				removeFromHand(card);
				return true;
			}
			else return false;
		}
		else return false;
	}
	
	public boolean equipRightWeapon(Card card){
		String card_class = card.getClass().getName();
		if(left_weapon != null){
			String left_weapon_class = left_weapon.getClass().getName();
			if(left_weapon_class.equals("Clash.TwoHandedWeapon") || card_class.equals("Clash.TwoHandedWeapon")){
				System.out.print("This will unequip the left weapon. Do you want to continue? (y/n) ");
				Scanner in = new Scanner(System.in);
				String response = in.next();
				if(response.equals("y")){
					if(left_weapon.getClass().getName().equals("Clash.OneHandedWeapon")){
						agility -= ((OneHandedWeapon)left_weapon).getAgility();
					}
					left_weapon = null;
				}
				else{
					choice = true;
					return false;
				}
			}
		}
		if(card_class.equals("Clash.Shield")){
			if(right_weapon != null){
				if(right_weapon.getClass().getName().equals("Clash.OneHandedWeapon")){
					agility -= ((OneHandedWeapon)right_weapon).getAgility();
				}
			}
			right_weapon = card;
			removeFromHand(card);
			return true;
		}
		if(card_class.equals("Clash.OneHandedWeapon")){
			if(right_weapon != null){
				if(right_weapon.getClass().getName().equals("Clash.OneHandedWeapon")){
					agility -= ((OneHandedWeapon)right_weapon).getAgility();
				}
			}
			right_weapon = card;
			agility += ((OneHandedWeapon)card).getAgility();
			removeFromHand(card);
			return true;
		}
		if(card_class.equals("Clash.TwoHandedWeapon")){
			if(right_weapon != null){
				if(right_weapon.getClass().getName().equals("Clash.OneHandedWeapon")){
					agility -= ((OneHandedWeapon)right_weapon).getAgility();
				}
			}
			left_weapon = card;
			right_weapon = card;
			removeFromHand(card);
			return true;
		}
		else{
			choice = false;
			return false;
		}
	}
	
	public boolean equipLeftWeapon(Card card){
		String card_class = card.getClass().getName();
		if(right_weapon != null){
			String right_weapon_class = right_weapon.getClass().getName();
			if(right_weapon_class.equals("Clash.TwoHandedWeapon")){
				System.out.print("This will unequip the right weapon. Do you want to continue? (y/n) ");
				Scanner in = new Scanner(System.in);
				String response = in.next();
				if(response.equals("y")){
					if(right_weapon.getClass().getName().equals("Clash.OneHandedWeapon")){
						agility -= ((OneHandedWeapon)right_weapon).getAgility();
					}
					right_weapon = null;
				}
				else{
					choice = true;
					return false;
				}
			}
		}
		if(card_class.equals("Clash.Shield")){
			if(left_weapon != null){
				if(left_weapon.getClass().getName().equals("Clash.OneHandedWeapon")){
					agility -= ((OneHandedWeapon)left_weapon).getAgility();
				}
			}
			left_weapon = card;
			removeFromHand(card);
			return true;
		}
		if(card_class.equals("Clash.OneHandedWeapon")){
			if(left_weapon != null){
				if(left_weapon.getClass().getName().equals("Clash.OneHandedWeapon")){
					agility -= ((OneHandedWeapon)left_weapon).getAgility();
				}
			}
			left_weapon = card;
			agility += ((OneHandedWeapon)card).getAgility();
			removeFromHand(card);
			return true;
		}
		if(card_class.equals("Clash.TwoHandedWeapon")){
			if(left_weapon != null){
				if(left_weapon.getClass().getName().equals("Clash.OneHandedWeapon")){
					agility -= ((OneHandedWeapon)left_weapon).getAgility();
				}
			}
			right_weapon = card;
			left_weapon = card;
			removeFromHand(card);
			return true;
		}
		else{
			choice = false;
			return false;
		}
	}
	
	public boolean equipRightLeg(Card card){
		String card_class = card.getClass().getName();
		if(card_class.equals("Clash.Armour")){
			if(((Armour)card).getType() == 4){
				if(right_leg != null){
					agility -= ((Armour)right_leg).getAgility();
				}
				right_leg = card;
				agility += ((Armour)card).getAgility();
				removeFromHand(card);
				return true;
			}
			else return false;
		}
		else return false;
	}
	
	public boolean equipLeftLeg(Card card){
		String card_class = card.getClass().getName();
		if(card_class.equals("Clash.Armour")){
			if(((Armour)card).getType() == 4){
				if(left_leg != null){
					agility -= ((Armour)left_leg).getAgility();
				}
				left_leg = card;
				agility += ((Armour)card).getAgility();
				removeFromHand(card);
				return true;
			}
			else return false;
		}
		else return false;
	}
	
	public boolean equipConjuration(Card card){
		String card_class = card.getClass().getName();
		if(card_class.equals("Clash.Conjuration")){
			conjuration = card;
			removeFromHand(card);
			return true;
		}
		else return false;
	}
	
	public boolean usePotion(Card card, Player from, Player to, boolean p1){
		String card_class = card.getClass().getName();
		if(card_class.equals("Clash.Potion")){
			if(((Potion)card).getType() == 1){
				from.offsetHealth(50);
			}
			
			if(((Potion)card).getType() == 2){
				from.offsetHealth(80);
			}
			
			if(((Potion)card).getType() == 3){
				from.offsetHealth(120);
			}
			
			if(((Potion)card).getType() == 4){
				from.offsetMana(100);
			}
			
			if(((Potion)card).getType() == 5){
				from.offsetMana(180);
			}
			
			if(((Potion)card).getType() == 6){
				from.offsetMana(300);
			}
			
			if(((Potion)card).getType() == 7){
				prev_agility = from.getAgility();
				if(p1){
					if (!enhanced_agility){
						enhanced_agility_end = Game.turnNo() + 3;
						enhanced_agility = true;
					}
				}
				else{
					if (!enhanced_agility){
						enhanced_agility_end = Game.turnNo() + 3;
						enhanced_agility = true;
					}
				}
				from.setAgility(agility + 10);
			}
			
			if(((Potion)card).getType() == 8){
				if(p1){
					to.setPoison(true);
				}
				else{
					to.setPoison(false);
				}
			}
			
			if(((Potion)card).getType() == 9){
				if(p1){
					to.setParalysis(true);
				}
				else{
					to.setParalysis(false);
				}
			}
			
			if(((Potion)card).getType() == 10){
				if(p1){
					to.setBrokenBone(true);
				}
				else{
					to.setBrokenBone(false);
				}
			}
			removeFromHand(card);
			return true;
		}
		else return false;
	}
	
	public void useConjuration(Card card, Player from, Player to, boolean p1){
		Random gen = new Random();
		from.offsetMana(-((Conjuration)card).getManaCost());
		int type = ((Conjuration)card).getType();
		int random = gen.nextInt(2);
		if(type == 4){
			if (random == 0) to.setPoison(p1);
		}
		if(type == 5){
			if (random == 0) to.setParalysis(p1);
		}
		if(type == 6){
			if (random == 0) to.setBrokenBone(p1);
		}
		removeFromHand(card);
	}
	
	public Card searchFor(String card){
		for(Card req_card : hand){
			if(req_card.getName().equals(card)){
				return req_card;
			}
		}
		return null;
	}
	
	public Card searchFor(int index){
		return hand.get(index - 1);
	}
	
	public void printEquipList(){
		String empty = "EMPTY";
		String heads = "Head";
		String torsos = "Torso";
		String left_arms = "Left arm";
		String right_arms = "Right arm";
		String left_weapons = "Left weapon";
		String right_weapons = "Right weapon";
		String left_legs = "Left leg";
		String right_legs = "Right leg";
		String conjurations = "Conjuration";
		
		if (head == null){
			System.out.printf("1. %-15s%s%n",heads,empty);
		}
		else{
			System.out.printf("1. %-15s%s - %s%n", heads, head.getName(), head.toString());
		}
		
		if (torso == null){
			System.out.printf("2. %-15s%s%n",torsos,empty);
		}
		else{
			System.out.printf("2. %-15s%s - %s%n", torsos, torso.getName(), torso.toString());
		}
		
		if (left_arm == null){
			System.out.printf("3. %-15s%s%n",left_arms,empty);
		}
		else{
			System.out.printf("3. %-15s%s - %s%n", left_arms, left_arm.getName(), left_arm.toString());
		}
		
		if (right_arm == null){
			System.out.printf("4. %-15s%s%n",right_arms,empty);
		}
		else{
			System.out.printf("4. %-15s%s - %s%n", right_arms, right_arm.getName(), right_arm.toString());
		}
		
		if (left_weapon == null){
			System.out.printf("5. %-15s%s%n",left_weapons,empty);
		}
		else{
			if(left_weapon.getClass().getName().equals("Clash.OneHandedWeapon")){
				System.out.printf("5. %-15s%s (one-handed) - %s%n", left_weapons, left_weapon.getName(), left_weapon.toString());
			}
			if(left_weapon.getClass().getName().equals("Clash.TwoHandedWeapon")){
				System.out.printf("5. %-15s%s (two-handed) - %s%n", left_weapons, left_weapon.getName(), left_weapon.toString());
			}
			if(left_weapon.getClass().getName().equals("Clash.Shield")){
				System.out.printf("5. %-15s%s (shield) - %s%n", left_weapons, left_weapon.getName(), left_weapon.toString());
			}
		}
		
		if (right_weapon == null){
			System.out.printf("6. %-15s%s%n",right_weapons,empty);
		}
		else{
			if(right_weapon.getClass().getName().equals("Clash.OneHandedWeapon")){
				System.out.printf("6. %-15s%s (one-handed) - %s%n", right_weapons, right_weapon.getName(), right_weapon.toString());
			}
			if(right_weapon.getClass().getName().equals("Clash.TwoHandedWeapon")){
				System.out.printf("6. %-15s%s (two-handed) - %s%n", right_weapons, right_weapon.getName(), right_weapon.toString());
			}
			if(right_weapon.getClass().getName().equals("Clash.Shield")){
				System.out.printf("6. %-15s%s (shield) - %s%n", right_weapons, right_weapon.getName(), right_weapon.toString());
			}
		}
		
		if (left_leg == null){
			System.out.printf("7. %-15s%s%n",left_legs,empty);
		}
		else{
			System.out.printf("7. %-15s%s - %s%n", left_legs, left_leg.getName(), left_leg.toString());
		}
		
		
		if (right_leg == null){
			System.out.printf("8. %-15s%s%n",right_legs,empty);
		}
		else{
			System.out.printf("8. %-15s%s - %s%n", right_legs, right_leg.getName(), right_leg.toString());
		}
		
		if (conjuration == null){
			System.out.printf("9. %-15s%s%n",conjurations,empty);
		}
		else{
			System.out.printf("9. %-15s%s - %s%n", conjurations, conjuration.getName(), conjuration.toString());
		}
	}
	
	public Card getCardAtIndex(int index){
		return hand.get(index);
	}
	
	public void printStatus(){
		System.out.println("Health: " + health + ", Mana: " + mana);
		System.out.println("Agility: " + agility);
		if(poisoned){
			System.out.println("Poisoned");
		}
		if(paralysed){
			System.out.println("Paralysed");
		}
		if(broken_bone){
			System.out.println("Broken bone");
		}
	}
}